# Data

This repository contains some datasets that I use in some of my experiments. The files are tracked by [Git LFS](https://docs.gitlab.com/ee/topics/git/lfs/).
